require 'rails_helper'

RSpec.describe "Categories", type: :system do
  describe 'カテゴリーページが表示されているとき' do
    let(:taxonomy) { create(:taxonomy, name: 'Categories') }
    let(:taxon1) { create(:taxon, name: 'Bugs', taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let(:taxon2) { create(:taxon, name: 'Mugs', taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let!(:product1) { create(:product, name: 'rails-bug', taxons: [taxon1]) }
    let!(:product2) { create(:product, name: 'rails-mug', taxons: [taxon2]) }
    let!(:product3) { create(:product, name: 'rails-stain', taxons: [taxon2]) }

    before do
      visit potepan_category_path(taxon1.id)
    end

    describe '商品カテゴリーに紐づくtaxonとproduct' do
      it 'タイトルがtaxon_idに合わせて動的に変更される' do
        expect(page).to have_title "#{taxon1.name} - BIGBAG Store"
        within '.taxonomy' do
          click_link taxon2.name
          expect(current_path).to eq potepan_category_path(taxon2.id)
          expect(page).to have_title "#{taxon2.name} - BIGBAG Store"
        end
      end

      it 'header下でtaxonに紐づいたnameが表示されている' do
        within '.page-title' do
          expect(page).to have_content taxon1.name
        end

        within '.active-taxon' do
          expect(page).to have_content taxon1.name
        end
      end

      it '商品カテゴリー部分でtaxonomyとtaxonが表示されている' do
        within '.category-panel' do
          expect(page).to have_content taxonomy.name
          click_link taxonomy.name
          expect(page).to have_content taxon1.name
          expect(page).to have_content '(1)'
          expect(page).to have_content taxon2.name
          expect(page).to have_content '(2)'
        end
      end

      it '商品カテゴリーのtaxonをクリックすると、taxonに紐づくcategory pageに移る' do
        within '.category-panel' do
          click_link taxonomy.name
          click_link taxon1.name
          expect(current_path).to eq potepan_category_path(taxon1.id)
        end
        expect(page).to have_content product1.name
        expect(page).not_to have_content product2.name
      end

      it 'taxonに紐づくproductのname display_priceが表示されている' do
        expect(page).to have_link product1.name
        expect(page).to have_link product1.display_price
        expect(page).to have_link 'products-img'
      end

      it '商品の詳細(name)をクリックすると、プロダクトページに移動する' do
        click_link product1.name
        expect(current_path).to eq potepan_product_path(product1.id)
      end

      it '商品の詳細(image)をクリックすると、プロダクトページに移動する' do
        click_link 'products-img'
        expect(current_path).to eq potepan_product_path(product1.id)
      end

      it '商品の詳細(price)をクリックすると、プロダクトページに移動する' do
        click_link product1.display_price
        expect(current_path).to eq potepan_product_path(product1.id)
      end
    end
  end
end
