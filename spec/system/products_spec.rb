require 'rails_helper'

RSpec.describe "Products", type: :system do
  describe 'productページが表示されている時' do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let(:taxon1) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:product1) { create(:product) }
    let!(:product2) { create(:product, taxons: [taxon, taxon1]) }
    let!(:product3) { create(:product, taxons: [taxon]) }
    let!(:product4) { create(:product, taxons: [taxon]) }
    let!(:product5) { create(:product, taxons: [taxon]) }
    let!(:product6) { create(:product, taxons: [taxon]) }
    let!(:product7) { create(:product, taxons: [taxon1]) }

    before do
      visit potepan_product_path(product.id)
    end

    it '期待される商品名、価格、説明が表示されている' do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
    end

    it 'header内のHomeリンクをふんだら、トップページに移動する' do
      within '.comparison1' do
        expect(page).to have_link 'Home', href: potepan_index_path
        click_link 'Home'
      end
      expect(page).to have_current_path potepan_index_path
    end

    it 'header下のHomeリンクをふんだら、トップページに移動する' do
      within '.comparison2' do
        expect(page).to have_link 'Home', href: potepan_index_path
        click_link 'Home'
      end
      expect(page).to have_current_path potepan_index_path
    end

    it 'header内のロゴをふんだら、トップページに移動する' do
      click_link 'BIGBAG'
      expect(page).to have_current_path potepan_index_path
    end

    context 'productがtaxonに属している場合' do
      it '一覧ページへ戻るリンクをクリックしたら、productに紐づくcategory pageに戻る' do
        expect(page).to have_link '一覧ページへ戻る'
        click_link '一覧ページへ戻る'
        expect(page).to have_current_path potepan_category_path(product.taxons.ids.first)
      end
    end

    context 'productがtaxonに属していない場合' do
      it 'product詳細ページ内部で,一覧ページへ戻るリンクが表示されていない' do
        visit potepan_product_path(product1.id)
        expect(page).not_to have_link '一覧ページへ戻る'
      end
    end

    it 'productが4つ表示されていること' do
      expect(page).to have_selector '.productBox', count: Constants::RELATED_PRODUCTS
    end

    it 'product自身と関連していないtaxonを持つproduct以外の関連商品が表示されている' do
      related_products = [product2, product3, product4, product5]
      within '.related_products' do
        related_products.each do |related_product|
          expect(page).to have_link related_product.name
          expect(page).to have_link related_product.display_price
          expect(page).to have_link 'products-img'
        end
        expect(page).not_to have_link product.name
        expect(page).not_to have_link product7.name
      end
    end

    it '関連商品部分のリンクを踏むと,productページに移動する' do
      within '.related_products' do
        click_link product2.name
        expect(page).to have_current_path potepan_product_path(product2.id)
      end
      expect(page).to have_content product2.name
    end
  end
end
