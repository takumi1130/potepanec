require 'rails_helper'

RSpec.describe Potepan::ProductsDecorator, type: :model do
  describe '#related_product' do
    let(:product) { create(:product, taxons: [taxon]) }
    let(:product2) { create(:product, taxons: [taxon, taxon1]) }
    let(:product3) { create(:product, taxons: [taxon]) }
    let(:product4) { create(:product, taxons: [taxon1]) }
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let(:taxon1) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }

    it 'product自身は含まないこと' do
      expect(product.related_product).not_to eq product
    end

    it 'productに紐づかないものは関連商品に含まないこと' do
      expect(product.related_product).not_to eq product4
    end

    it 'productにひもづく関連商品データが取れていること' do
      expect(product.related_product).to contain_exactly(product2, product3)
    end
  end
end
