require 'rails_helper'

RSpec.describe "Categories", type: :request do
  describe 'GET #show' do
    let(:taxonomy) { create(:taxonomy, name: 'Categories') }
    let(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let!(:product1) { create(:product, name: 'rails-bug', taxons: [taxon]) }
    let!(:product2) { create(:product, name: 'rails-tote', taxons: [taxon]) }

    before do
      get potepan_category_path(taxon.id)
    end

    it '正常なレスポンスが返ってくる' do
      expect(response.status).to eq 200
    end

    it '期待されるページが表示されている' do
      expect(response).to render_template(:show)
    end

    it 'taxonomy名が表示される' do
      expect(response.body).to include taxonomy.name
    end

    it 'taxon名が表示される' do
      expect(response.body).to include taxon.name
    end

    it 'product名が表示される' do
      expect(response.body).to include product1.name
      expect(response.body).to include product2.name
    end
  end
end
