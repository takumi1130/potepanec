require 'rails_helper'

RSpec.describe "Products", type: :request do
  describe 'GET #show' do
    let(:product) { create(:product, taxons: [taxon]) }
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let!(:product2) { create(:product, taxons: [taxon]) }
    let!(:product3) { create(:product, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it 'リクエストが成功すること' do
      expect(response.status).to eq 200
    end

    it '個別productページが表示されていること' do
      expect(response).to render_template(:show)
    end

    it 'idにひもづくproduct名が表示されていること' do
      expect(response.body).to include product.name
    end

    it 'productのtaxonにひもづく関連商品が取れていること' do
      expect(response.body).to include product2.name
      expect(response.body).to include product3.name
    end
  end
end
