module Potepan::ProductsDecorator
  def related_product
    Spree::Product.in_taxons(taxons).
      includes(master: [:images, :default_price]).
      distinct.
      where.not(id: id)
  end
  Spree::Product.prepend self
end
